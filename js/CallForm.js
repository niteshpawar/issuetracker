function CallForm(path, attr = '#repo-overview-link'){
    $.ajax({
	    url: path,
		success: function(data) {
		$('#content').html(data);
	    }
	});
    $('.aui-nav').children().removeAttr('class');
    $(attr).parent().attr('class','aui-nav-selected');

}

$(document).ready(function() {

	$('#repo-issues-link').click(function(){
	    	CallForm('View/Issues.php',this);
	    });
	$('#repo-overview-link').click(function(){
	    	CallForm('View/Overview.php',this);
	    });

    });