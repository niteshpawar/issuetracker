<?php 
if(session_id() == ''){
    $_SESSION['user'] = TRUE;
  }
if(isset($_GET['destination'])){
  if($_GET['destination'] == 'Logout'){
    $_SESSION['user'] = FALSE;
  }
}
?>
<html>
<head>
<script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.8.11.custom.min.js" type="text/javascript"></script>
<script src="js/CallForm.js" type="text/javascript"></script>
<link type="text/css" href="css/style.css" rel="stylesheet">
<link type="text/css" href="css/bitbucket.css" rel="stylesheet">
</head>
<body>
<div id="page">
<div id="wrapper">
<header id="header" role="banner">
   <nav class="aui-header aui-dropdown2-trigger-group" role="navigation">
   <div class="aui-header-inner">
   <div class="aui-header-primary">
   <h1 class="aui-header-logo aui-header-logo-bitbucket">
   <a class="aui-nav-imagelink" href="/">
   <span class="aui-header-logo-device">Bitbucket</span>
   </a>
   </h1>
   </div>
   </div>
   </nav>
   </header>
<?php
require_once 'View/Header.php';
?>

<?php
if (!$_SESSION['user']) {
  require_once 'View/login.php'; 
}else{ 
  require_once 'View/Menu.php';
  (isset($_GET['destination'])) ?  require_once 'View/'.$_GET['destination'].'.php' : require_once 'View/Landing.php'; 
}
?>

</div>
<?php
require_once 'View/footer.php';
?>
</div>
</body>
</html>