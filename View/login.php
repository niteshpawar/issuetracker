<?php
print_r($_POST);
?>    
  <div id="content" role="main" style=" padding-top: 40px !important;">
  <div data-method="standard" class="login-box workflow-box small" id="login">
    <header>
      <h1>Log in</h1>
     
    </header>
    <section class="form-container">  

<div>
  <form id="login-form" method="post" action="" class="aui login-form">
      
        <div class="field-group username">
          <label for="id_username">Username or email</label>
          <input type="text" autofocus="autofocus" name="username" id="id_username" class="text">
        </div>
      
    
    
      <div class="field-group">
        <label for="id_password">Password</label>
        <input type="password" name="password" class="password" id="id_password">
      </div>
    <div class="buttons-container">
      <div class="buttons">
        <button class="aui-button aui-style aui-button-primary" name="submit" type="submit">
          
            Log in
          
        </button>
        <a class="forgot-password" href="/account/password/reset/">Forgot your password?</a>
      </div>
      
        <div class="sign-up">
          <a href="/account/signup/">Need an account? Sign up free.</a>
        </div>
      
    </div>
  </form>
</div>
 
    </section>
  </div>

 </div>